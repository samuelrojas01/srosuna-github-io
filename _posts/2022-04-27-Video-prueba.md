---
layout: post
title: Flashear viejo Router TP Link Modelo WR841N al DD-WRT
date: 2022-04-27
author: Samuel R Osuna.
categories: Blogging, Router, Tp-link, WR841N, DD-WRT, Floss, Software, Libre, OpenSource
---

Estos días requerí actualizar mi viejo Router porque estaba presentando fallas, la verdad fue muy rápido y mejor les dejo el video de como hacerlo

[youtube](https://www.youtube.com/watch?v=XgWt2h6iKSA)

Gracias por verlo.
