---
layout: post
title: Generando Certificados y Diplomas Con Python3 Para Eventos y Conferencias
date: 2022-04-27
author: Samuel R Osuna.
categories: Blogging, Eventos, Conferencias, Software, Libre, Freesoftware, foss, floss, python, programación, Certificados de asistencia, python3, Diplomas 
---

El pasado día 23 de Abril de 2022, durante la celebración del pasado Flisol, el cual ya comenté en una [entrada anterior](https://srosuna.github.io/Flisol-Barquisimeto/) mostré a los asistentes algunos código de los cuales he trabajado.

Para ir al grano, les presento este proyecto que solo le hice unas pequeñas modificaciones, con respecto al [código original](https://dev.to/rahulsinha036/generate-certificate-using-python-43fa) que le pertenece a Rahul Sinha y que pueden clonar desde su [github](https://github.com/rahulsinha036)

Mi modificación no se diferencia mucho de desarrollador original, la mía la framifiqué en cuatro tipos de uso, las cuales están diseñadas para trabajar desde dos fuentes diferentes y estos son:

**Archivos XLXS**

Exactamente del formato de Microsoft Office, y pueden generar los certificados en archivos con extensión PDF o si gustan en imágenes JPG.

**Archivos ODS de Libreoffice**

Como obviamente estoy más a favor del Software Libre y el Open Source o FLOSS no puedo dejar de lado mi ofimática de mayor uso que es [LibreOffice](https://www.libreoffice.org/),

En este caso, tuve que agregar un engine de odf como está en el código.

>data = pd.read_excel (r'gen-4.ods', **engine="odf"**)

Es necesario instalar con pip3 odfpy

> $pip3 install odfpy

Al igual que las anteriores scripts con fuente de archivos en Microsoft Excel, estas pueden generar en archivo con extensión  pdf asi como jpg.


**Generación en carpetas independiente**

A diferencia del código original, añadí una linea para que se generen los certificados no en la raíz del proyecto, sino en una carpeta independiente dentro de la raíz, el usuario debe antes crear dicha carpeta según sea su gusto. donde generar dos tipos de formatos deseados, en Linux esto se hace en consola con:

>$ mkdir jpg/

o  si lo desea en PDF...

> $mkdir pdf/

Es recomendable hacerlo en la raiz del proyecto.

Estoy tabajando en la creación de otra Script donde la fuente sea en formato csv, o archivos separados por comas.

Los archivos disponibles con las scripts, son estos:

>$ ls *.py
certificate-generator-xlsx-jpg.py 
certificate-generator-ods-jpg.py  
certificate-generator-xlsx-pdf.py 
certificate-generator-ods-pdf.py

Creo que se explican por si mismos cada uno de ellos.

Para ejecutarlos, simplemente haga desde la consola por ejemplo si desea generar desde un xlsx a un pdf:

>python3 certificate-generator-xlsx-pdf.py

La documentación total del archivo está en el Reame.md y está en idioma inglés.

Pueden verlo en mi github, háganle clone, fork, mejorarlo, etc.

[https://github.com/srosuna/attendance-certificates-python3/tree/master](https://github.com/srosuna/attendance-certificates-python3/tree/master)


Por favor comparte y deja tu comentario y cualquier duda trataré de contestarla.


