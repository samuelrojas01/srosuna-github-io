---
layout: post
title: Script para generar certificados de asistencia desde una hoja de cálculo-
date: 2022-04-27
author: Samuel R Osuna.
categories: Blogging, Eventos, software, libre, foss, python, programación, certificados de asistencia, python3 
---

El pasado día 23 de Abril de 2022, se celebró en la ciudad de Barquisimeto el Festival Latinoaméricano de Instalación de Software Libre, mejor conocido como Flisol

![_config.yml]({{ site.baseurl }}/images/articulo_flisol2022/Flisol-2015.png)

**Breve Reseña de la pagina del [Flisol](https://flisol.info/)  **

> **El FLISoL es el evento de difusión de Software Libre más grande en Latinoamérica y está dirigido a todo tipo de público: estudiantes, académicos, empresarios, trabajadores, funcionarios públicos, entusiastas y aun personas que no poseen mucho conocimiento informático.. **

Desde hace varios años, por motivos de la situación económica del país, así como los efectos de la pandemia, este evento no se estaba organizando, para ser exactos desde el año 2016 hasta el 2021. en alguinos casos por falta de empatía de sus antiguos organizadores, colaboración y luego la alta migración de algunas de estas personas que de por si, ya no ayudaban en este evento, así como después los efectos de la inflación y posterior pandemia venida entre los años 2019 hasta el 2021.

Sin embargo, en 2022, por iniciativa de amigo Edery Rodriguez [@ejavierds](https://twitter.com/ejavierds), tuvo la iniciativa de revivir este evento que ya hacía falta su celebración, en este evento se dieron varias charlas y talleres, con todas las medidas de bioseguridad respectivas ante la lucha del Covid-19.

** Taller de Python **
En lo que a mi respecta, quise participar con varias charlas, había propuesto tres, como son, **Herramientras de edición multimedia**; **Instalación de un servidor web para páginas estáticas**; y finalmente **Python for Dummies**, el comité quiso que diese la tercera charla, y me fue asignada a la 1pm.

A pesar de los problemas logísticos que se me presentaron (mi vehículo se accidentó, problemas de transporte, etc) pude llegar justo a la hora, y con la ayuda de mi amigo y compañero [@oteroweb](https://www.instagram.com/oteroweb/), que me ayudo con las diapositivas, pudimos dar entre los dos la charla a la gente que estaba el aula al lleno total. 

Nos quedamos sorprendidos al ver que tuvieron que abrir la puerta para que entrara más gente que quisiero ver esta charla, ya que Edery nos comentó que fue la más demandada en la inscripción, donde mas que un taller de aplicaciónes, esta charla estaba destinada a incentivar a la gente que se inicia en el mundo de la programación, el porqué deben tomar en cuenta a Python como su primer lenguaje, les mostré algunas script que he modificado de otros, así como trabajos propios y ejemplos donde está siendo usado Python como por ejemplo en el descubrimiento del [agujero negro](https://www.bbc.com/mundo/noticias-47880446) y su relación con [python](https://programmerclick.com/article/2352896082/) , y donde mostré también el codigo del proyecto [eht-imaging](https://github.com/achael/eht-imaging) para que vieran como estaba estructurado el código fuente del mismo.


en el [Telescopio Espacial James Webb](https://www.youtube.com/watch?v=LmZ8w9WLEYY)y muchas experiencias mas.

Lastimosamente, por la premura, no pudimos hacer un live de la charla, fue mas que todo por el apuro pero la gente creo que disfruto nuestro taller, tanto así que esos jóivenes y profesores que estaban en el aula, me pidieron que les dejara la presentación la cual dejo a continuación en formato PDF para ser descargada.

**Descarga la presenteción aquí** 
> [Presentación Python for Dummies](https://drive.google.com/file/d/1knc63X_mtInU5uA8HC4JLLnLYSChP-b3/view?usp=sharing)


** Apreciación final **
Me hubiese gustado tener más tiempo,para dar una demostración mostrar como programar en Docker, Django, y el uso de algunos IDE's y frameworks pero será para otra oportunidad podré hacer este tipo de talleres más avanzados a la gente porque en verdad siento que hay mucho interés por aprende más software libre y programación en FLOSS.

Finalmente, me dijeron los organizadores que este sería el primero de varios eventos por venir, como el Dia Debian, quizás retomen el CNSL, Software Freedom Day, etc, donde realmente se aporte más conocimientos y no como otras personas que en el pasado lo que hacían eran reunirse para hacer parrilladas y perder el tiempo y al final no aportaban nada.

** Agradecimientos **

Les quiero con mucha humildad dar las gracias a los organizadores del evento por su invitación y que estaré atento para futuras invitaciones asó como colaborar con otros eventos que se organicen a futuro.
 
** Algunas fotos **

***Antes de iniciar la charla***

![_config.yml]({{ site.baseurl }}/images/articulo_flisol2022/photo_2022-04-23_14-59-05.jpg)

*** Un merecido almuerzo ***
![_config.yml]({{ site.baseurl }}/images/articulo_flisol2022/photo_2022-04-23_14-59-04.jpg)

***Foto Grupal con algunos organizadores ***

![_config.yml]({{ site.baseurl }}/images/articulo_flisol2022/post_flisol.jpg)

**Foto resumen **
![_config.yml]({{ site.baseurl }}/images/articulo_flisol2022/photo_2022-04-22_42-05.jpg)



